import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import ShoesForm from './ShoeForm';
import ShoesList from './ShoeList';
import React from 'react';
import './index.css';


class App extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      hats: [],
      shoes: [],
    };
    this.loadHats = this.loadHats.bind(this);
    this.loadShoes = this.loadShoes.bind(this);
    this.deleteShoe = this.deleteShoe.bind(this);
    this.deleteHat = this.deleteHat.bind(this);
  }

  async componentDidMount() {
    this.loadHats()
    this.loadShoes()
  }

  async loadHats() {
    const response = await fetch("http://localhost:8090/api/hats");
    if(response.ok) {
      const data = await response.json();
      this.setState({hats: data.hats});
    }
  }

  async loadShoes() {
    const response = await fetch("http://localhost:8080/api/shoes");
    if(response.ok) {
      const data = await response.json();
      this.setState({shoes: data.shoes});
    }
  }

  async deleteShoe (shoe) {
    if (window.confirm("Are you sure you want to delete this?")) {
      const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
      const fetchConfig = {
        method: 'delete',
      }
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoes = this.state.shoes.filter((sh) => shoe.id != sh.id)
      this.setState({shoes: newShoes})
    }
    }
  }

  async deleteHat (hat) {
    if (window.confirm("Are you sure you want to delete this?")) {
      const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
      const fetchConfig = {
        method: 'delete',
      }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHats = this.state.hats.filter((h) => hat.id != h.id)
      this.setState({hats: newHats})
    }
    }
  }

  render() {
    return(
      <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="home" element={<MainPage />} />
            <Route index element={<MainPage />} />
          <Route path="hats" element={<HatsList hats={this.state.hats} delete={this.deleteHat}/>} />
          <Route path="hats">
            <Route path="new" element={<HatsForm hats={this.state.hats}/>} />
          </Route>
          <Route path="shoes" element={<ShoesList shoes={this.state.shoes} delete={this.deleteShoe}/>} />
          <Route path="shoes">
            <Route path="new" element={<ShoesForm shoes={this.state.shoes}/>} />
          </Route>
        </Routes>
    </BrowserRouter>
    );
  }
}

export default App;


// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import MainPage from './MainPage';
// import Nav from './Nav';
// import React from 'react';
// import ShoesForm from './ShoesForm';
// import ShoesList from './ShoesList';
// import HatsList from './HatsList';
// import HatForm from './HatsForm';

// function App(props) {
//   if (props.hats === undefined) {
//     return null
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//         <Route path="shoes">
//             <Route path="new" element={<ShoesForm />} />
//             <Route path="" element={<ShoesList />} />
//           </Route>
//           <Route path="/" element={<MainPage />} />
//           <Route path="hats">
//             <Route path="" element={<HatsList hats={props.hats} /> } />
//             <Route path="new" element={<HatForm /> } />
//           </Route>
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }

// export default App;